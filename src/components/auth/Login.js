import React, {useState, useContext} from 'react';
import {Link} from 'react-router-dom';
import AlertaContext from '../../context/alerta/alertaContext'
import AuthContext from '../../context/autentificacion/authContext'

const Login = () => {

    // extraer los valores del context
    const alertaContext = useContext(AlertaContext);
    const { alerta, mostrarAlerta } = alertaContext;

    //extraer los valores del auth context
    const authContext = useContext(AuthContext);
    const {mensaje, autenticado, iniciarSesion } = authContext;

    const [usuario, guardarUsuario]= useState({
        email: '',
        password: ''
    });

    console.log(usuario);

    const {email, password} = usuario;

    const onChange = e => {
        guardarUsuario({
            ...usuario,
            [e.target.name] : e.target.value
        })
    }


    //cuando el usario quiere iniciar sesion

    const onSubmit = e => {
        e.preventDefault();

        //validar que no haya campos vacios 
        if(email.trim() === '' || password.trim() === ''){
            mostrarAlerta('Todos los cmapos son obligatorios', 'alerta-error');
            return;                        
        }
        //pasarlo al action
        iniciarSesion({email, password});
    }
    return (
        <div className="form-usuario">
        { alerta ? (<div className={`alerta ${alerta.categoria}`}>{alerta.msg}</div>) : null }
            <div className="contenedor-form sobra-dark">
                <h1>Iniciar Sesion</h1>
                <form
                    onSubmi={onSubmit}
                >
                    <div className="campo-form">
                        <label htmlFor="email">Email</label>
                        <input
                            type="email"
                            id="email"
                            name="email"
                            placeholder="Tu Email"
                            value={email}
                            onChange={onChange}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            id="password"
                            name="password"
                            placeholder="Tu Password"
                            value={password}
                            onChange={onChange}
                        />
                    </div>
                    <div className="campo-form">
                        <button
                            type="submit"
                            className="btn btn-primario btn-block"
                            value="Iniciar Sesion"
                        >Iniciar Sesion</button>
                    </div>
                </form>
                <Link to={'/nueva-cuenta'} className="enlace-cuenta">
                    Obtener Cuenta
                </Link>
            </div>
        </div>
    );
}

export default Login;