import React, { useContext, useState, useEffect } from 'react'
import proyectoContext from '../../context/proyectos/proyectoContext'
import tareaContext from '../../context/tareas/tareaContext'

const FormTarea = () => {
	//obtener el state del formulario
	const proyectosContext = useContext(proyectoContext)
	const { proyecto } = proyectosContext

	//obtenerlas tareas del proyecto
	const tareasContext = useContext(tareaContext)
	const { tareaseleccionada, errortarea, agregarTarea, validarTarea, obteneTareas, editarTarea, limpiarSeleccion } = tareasContext

	useEffect(()=>{
		if(tareaseleccionada !== null){
			guardarTarea(tareaseleccionada)
		}else{
			guardarTarea({
				nombre:''
			})
		}

	}, [tareaseleccionada])

	//creamos el state para guardar el nombre de la tarea
	const [ tarea, guardarTarea ] = useState({
		nombre: ''
	})
	const { nombre } = tarea

	//verficamos cuando no tena ningun proyecto seleccionado
	if (!proyecto) return null
	//realizamos un destracturing del proyecto
	const [ proyectoActual ] = proyecto

	//obteniedo los cambios
	const handleChange = e => {
		guardarTarea({
			...tarea,
			[e.target.name]: e.target.value
		})
	}

	const onSubmitForm = e => {
		e.preventDefault()

		//validar formulario
		if (nombre.trim() === '') {
			validarTarea()
			return
		}

		if(tareaseleccionada === null){
			//agregar una nueva tarea la state de tareas
			tarea.proyectoId = proyectoActual.id
			tarea.estado = false
			agregarTarea(tarea)
		}else{
			editarTarea(tarea);
			//limpiar la selecion
			limpiarSeleccion();
		}

		//obtener las tareas
		obteneTareas(proyectoActual.id)

		//reiniciar
		guardarTarea({
			nombre: ''
		})
	}
	return (
		<div className="formulario">
			<form onSubmit={onSubmitForm}>
				<div className="contenedor-input">
					<input
						type="text"
						className="input-text"
						value={nombre}
						onChange={handleChange}
						placeholder="Nombre Tarea"
						name="nombre"
					/>
				</div>
				<div className="contenedor-input">
					<button type="submit" className="btn btn-primario btn-submit btn-block" value="Agregar Tarea">
						{tareaseleccionada ? "Editar Tarea" : "Agregar Tarea"}
					</button>
				</div>
			</form>
			{errortarea ? <p className="mensaje error">El nombre de la tarea es obligatorio</p> : null}
		</div>
	)
}

export default FormTarea
