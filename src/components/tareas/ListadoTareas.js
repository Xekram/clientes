import React, { Fragment, useContext } from 'react'
import Tarea from './Tarea'
import proyectoContext from '../../context/proyectos/proyectoContext'
import tareaContext from '../../context/tareas/tareaContext'
import {CSSTransition, TransitionGroup} from 'react-transition-group';

const ListadoTareas = () => {
	//extraer proyectos del state inicial
	const proyectosContext = useContext(proyectoContext)
	const { proyecto, eliminarProyecto } = proyectosContext

	//obtenerlas tareas del proyecto
	const tareasContext = useContext(tareaContext)
	const { tareasproyecto } = tareasContext

	//si no hay proyecto seleccionado
	if (!proyecto) return <h2>Selecciona un proyecto</h2>

	//realizar el destructuring al arreglo
	const [ proyectoActual ] = proyecto

	//elimar un proyecto
	const onClickEliminar = () => {
		eliminarProyecto(proyectoActual.id)
	}
	return (
		<Fragment>
			<h2>Proyecto: {proyectoActual.nombre}</h2>
			<ul className="listado-tareas">
				{tareasproyecto.length === 0 ? (
					<li className="tarea">
						<p>No hay Tareas</p>
					</li>
				) : 
					<TransitionGroup>
						{
							(
								tareasproyecto.map(tarea => 
									<CSSTransition
										timeout={600}
										key={tarea.id}
										classNames="tarea"
									>
												<Tarea 
													tarea={tarea} 
												/>
									</CSSTransition>
								)
							)
						}
					</TransitionGroup>
				}
			</ul>
			<button onClick={onClickEliminar} type="button" className="btn btn-eliminar">
				Eliminar Proyecto &times;
			</button>
		</Fragment>
	)
}

export default ListadoTareas
