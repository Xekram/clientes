import React, {useContext} from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext'
import tareaContext from '../../context/tareas/tareaContext'

const Tarea = ({ tarea }) => {
	//extraer proyectos del state inicial
	const proyectosContext = useContext(proyectoContext)
	const { proyecto } = proyectosContext

	//obtenerlas tareas del proyecto
	const tareasContext = useContext(tareaContext)
	const { eliminarTarea, obteneTareas, estadoTarea, tareaActual } = tareasContext

	//ectraer del areglo de proyecto
	const [proyectoActual] = proyecto;

	// Funcion para eliminar tarea por el id 
	const tareaEliminar = id => {
		eliminarTarea(id);
		obteneTareas(proyectoActual.id);

	}

	//funcion para cambiar el estado de las tareas
	const cambiarEstado = tarea => {
		if(tarea.estado){
			tarea.estado = false
		}else{
			tarea.estado = true
		}
		estadoTarea(tarea)
	}

	//Seleccionar la tarea actual 
	const selecionarTarea = tarea => {
		tareaActual(tarea);
	}

	return (
		<li className="tarea sombra">
			<p>{tarea.nombre}</p>
			<div className="estado">
				{tarea.estado ? (
					<button 
						type="button" 
						className="completo"
						onClick={() => cambiarEstado(tarea)}
					>
						Completo
					</button>
				) : (
					<button  
						type="button" 
						className="incompleto"
						onClick={() => cambiarEstado(tarea)}
					>
						Incompleto
					</button>
				)}
			</div>
			<div className="acciones">
				<button 
					type="button" 
					className="btn btn-primario"
					onClick={() => selecionarTarea(tarea)}
				>
					Editar
				</button>

				<button 
				onClick={() => tareaEliminar(tarea.id)}
				type="button" 
				className="btn btn-secundario">
					Eliminar
				</button>
			</div>
		</li>
	);
};

export default Tarea;
