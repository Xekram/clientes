import React, {Fragment, useState, useContext}from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';

const NuevoProyecto = () => {

    //obtener el state del formulario
    const proyectosContext = useContext(proyectoContext)
    const {formulario, errorformulario , mostrarFormulario , agregarProyecto, mostrarError } = proyectosContext;

    // State para el Proyecto
    const [proyecto, guardarProyecto] = useState({
        nombre:''
    });
    
    // extraer el proyecto

    const {nombre} = proyecto;

    const onChangeProyecto = e => {
        guardarProyecto({
            ...proyecto,
            [e.target.name] : e.target.value
        })
    }

    //cuando el usuario envia un proyecto
    const onSubmitProyecto = e => {
        e.preventDefault();


        //validar el proyecto
        if(nombre === ''){
            mostrarError();
            return;
        }

        //agregar al state
        agregarProyecto(proyecto);

        //reiniciar el form 
        guardarProyecto({
            nombre: ''
        })
    }
    const onClickFormlario = () => {
        mostrarFormulario()
    }
    return (
        <Fragment>
            <button
            type="button"
            className="btn btn-block btn-primario"
            onClick={onClickFormlario}
            >Nuevo Proyecto</button>

            {formulario
                ?
                    (
                                <form
                                    className="formulario-nuevo-proyecto"
                                    onSubmit={onSubmitProyecto}
                                >
                                    <input
                                        type="text"
                                        className="input-text"
                                        placeholder="Nombre del Proyecto"
                                        name="nombre"
                                        value={nombre}
                                        onChange={onChangeProyecto}
                                    />

                                    <button
                                        type="submit"
                                        className="btn btn-primario btn-block"
                                        value="Agregar Proyecto"
                                    >
                                        Crear Proyecto
                                    </button>

                                </form>
                    )
                : null
            }
            {errorformulario ? <p className="error mensaje">El nombre del proyecto es necesario</p>:null}
        </Fragment>
    );
}

export default NuevoProyecto;