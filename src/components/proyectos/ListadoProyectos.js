import React, { useContext, useEffect } from 'react'
import Proyecto from './Proyecto'
import proyectoContext from '../../context/proyectos/proyectoContext'
import { CSSTransition, TransitionGroup } from 'react-transition-group'

const ListadoProyectos = () => {
	//extraer proyectos del state inicial
	const proyectosContext = useContext(proyectoContext)
	const { proyectos, obtenerProyectos } = proyectosContext

	//obtener proyectos cuando carga el componente
	useEffect(() => {
		obtenerProyectos();
		//eslint-disable-next-line
	}, [])

	if (proyectos.length === 0) return <p>No hay proyecto, comienza creano uno</p>

	return (
		<ul className="listado-proyectos">
			<TransitionGroup>
				{proyectos.map(proyecto => 
					<CSSTransition
						timeout={600}
						classNames="proyecto"
						key={proyecto.id} 
					>
						<Proyecto 
							proyecto={proyecto} 
						/>
					</CSSTransition>
				)}
			</TransitionGroup>
		</ul>
	)
}

export default ListadoProyectos
