import React, { useReducer } from 'react'
import TareaContext from './tareaContext'
import TareaReducer from './tareaReducer'
import { v4 as uuid } from 'uuid'
import { 
	TAREAS_PROYECTO, 
	AGREGAR_TAREA, 
	VALIDAR_TAREA, 
	ELIMINAR_TAREA,
	ESTADO_TAREA,
	TAREA_ACTUAL,
	ACTUALIZAR_TAREA,
	LIMPIAR_TAREA
 } from '../../types'

const TareaState = props => {
	const initialState = {
		tareas: [
			{ id:1, nombre: 'Elegir Plataforma', estado: true, proyectoId: 1 },
			{ id:2, nombre: 'Elegir Colores', estado: false, proyectoId: 2 },
			{ id:3, nombre: 'Elegir Plataforma de pago', estado: false, proyectoId: 3 },
			{ id:4, nombre: 'Elegir Hosting', estado: true, proyectoId: 4 },
			{ id:5, nombre: 'Elegir Plataforma weving', estado: true, proyectoId: 2 },
			{ id:6, nombre: 'Elegir Colores RGB', estado: false, proyectoId: 3 },
			{ id:7, nombre: 'Elegir Plataforma de joder', estado: false, proyectoId: 1 },
			{ id:8, nombre: 'Elegir Hosting HTML', estado: true, proyectoId: 2 },
			{ id:9, nombre: 'Elegir Plataforma OMS', estado: true, proyectoId: 2 },
			{ id:10, nombre: 'Elegir Colores data', estado: false, proyectoId: 3 },
			{ id:11, nombre: 'Elegir Plataforma de pago para paypal', estado: false, proyectoId: 1 },
			{ id:12, nombre: 'Elegir Hosting Donald Trump', estado: true, proyectoId: 2 },
			{ id:13, nombre: 'Elegir Plataforma para HTML', estado: true, proyectoId: 4 },
			{ id:14, nombre: 'Elegir Colores SGV', estado: false, proyectoId: 3 },
			{ id:15, nombre: 'Elegir Plataforma de PAYPAY', estado: false, proyectoId: 3 },
			{ id:16, nombre: 'Elegir Hosting made china', estado: true, proyectoId: 2 }
		],
		tareasproyecto: null,
		errortarea: false,
		tareaseleccionada: null
	}

	// crear el dispach y el state
	const [ state, dispach ] = useReducer(TareaReducer, initialState)

	//crear las funciones

	//obtener las tareas
	const obteneTareas = proyectoId => {
		dispach({
			type: TAREAS_PROYECTO,
			payload: proyectoId
		})
	}

	//agregar tareas a un proyecto
	const agregarTarea = tarea => {
		tarea.id = uuid();
		
		dispach({
			type: AGREGAR_TAREA,
			payload: tarea
		})
	}

	//validar si el usuario ingrega el campo vacio
	const validarTarea = () => {
		dispach({
			type: VALIDAR_TAREA
		})
	}

	//eliminar tarea x id
	const eliminarTarea = id => {
		dispach({
			type: ELIMINAR_TAREA,
			payload: id
		})
	}

	//cambiar el estado de las tareas
	const estadoTarea = tarea => {
		dispach({
			type: ESTADO_TAREA,
			payload: tarea
		})
	}

	//Extraer la tarea seleccionada 
	const tareaActual = tarea => {
		dispach({
			type: TAREA_ACTUAL,
			payload: tarea
		})
	}

	//editar la tarea selecionada
	const editarTarea = tarea => {
		dispach({
			type: ACTUALIZAR_TAREA,
			payload: tarea
		})
	}

	//limpiar la tarea selecionada
	const limpiarSeleccion = () => {
		dispach({
			type: LIMPIAR_TAREA
		})
	}


	//retornar el context para que se pueda usar
	return (
		<TareaContext.Provider
			value={{
				tareas: state.tareas,
				tareasproyecto: state.tareasproyecto,
				errortarea: state.errortarea,
				tareaseleccionada:state.tareaseleccionada,
				obteneTareas,
				agregarTarea,
				validarTarea,
				eliminarTarea,
				estadoTarea,
				tareaActual,
				editarTarea,
				limpiarSeleccion
			}}
		>
			{props.children}
		</TareaContext.Provider>
	)
}

export default TareaState
